package infra.processors.injection

import com.squareup.javapoet.*
import com.vdurmont.semver4j.Semver
import infra.processors.AbstractMixinGeneratorProcessor
import javax.annotation.processing.SupportedAnnotationTypes
import javax.annotation.processing.SupportedOptions
import javax.lang.model.element.*
import javax.lang.model.type.MirroredTypesException
import javax.lang.model.type.TypeMirror
import javax.tools.Diagnostic
import javax.tools.StandardLocation


@SupportedAnnotationTypes("infra.processors.injection.GenerateInjections")
@SupportedOptions("mc_version")
class GenerateInjectionsProcessor : AbstractMixinGeneratorProcessor(GenerateInjections::class) {

    companion object {
        private const val GEN_MIXIN_PACKAGE_NAME = "infra.mixin"
    }

    /**
     * Generate a mixin file for one specific generator annotation instance
     *
     * @param dummy the element that is annotated with [GenerateInjections]
     * @param semanticVersion the semantic version
     */
    override fun generateMixins(dummy: Element, semanticVersion: Semver) {
        // obtain class surrogate of the class given in annotation value

        val typeMirrors = try {
            dummy.getAnnotation(GenerateInjections::class.java).value
            throw AssertionError("the previous call already must fail")
        } catch (e: MirroredTypesException) {
            e.typeMirrors.map(processingEnv.typeUtils::asElement)
        }

        val generatedClasses = mutableListOf<String>()

        typeMirrors.forEach { injectedInterfaceClassElement ->
            val injectionTargetTypeMirror = try {
                mapToRuntimeType(injectedInterfaceClassElement.asType(), semanticVersion)
            } catch (e: IllegalArgumentException) {
                processingEnv.messager.printMessage(
                    Diagnostic.Kind.ERROR,
                    "${injectedInterfaceClassElement.asType()} points to a non-existent platform type ${e.message}"
                )
                return
            }
            val injectionTargetTypeElement =
                mapToRuntimeElement(injectedInterfaceClassElement.asType(), semanticVersion)

            val mixinClassName = injectedInterfaceClassElement.simpleName.toString().removePrefix("Infra")

            val mixinClassBuilder =
                if (injectionTargetTypeElement.kind == ElementKind.INTERFACE) {
                    TypeSpec.interfaceBuilder(mixinClassName)
                        .addModifiers(Modifier.PUBLIC)
                        .addAnnotation(
                            AnnotationSpec.builder(ClassName.get("org.spongepowered.asm.mixin", "Mixin"))
                                .addMember("value", "$injectionTargetTypeMirror.class")
                                .build()
                        )
                        .addAnnotation(
                            AnnotationSpec.builder(ClassName.get("org.spongepowered.asm.mixin", "Implements"))
                                .addMember(
                                    "value", "@\$T(iface = \$T.class, prefix=\$S, remap=\$T.ONLY_PREFIXED)",
                                    ClassName.get("org.spongepowered.asm.mixin", "Interface"),
                                    ClassName.get(injectedInterfaceClassElement as TypeElement),
                                    "proxy$",
                                    ClassName.get("org.spongepowered.asm.mixin", "Interface.Remap")
                                )
                                .build()
                        )
                } else {
                    TypeSpec.classBuilder(mixinClassName)
                        .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                        .addAnnotation(
                            AnnotationSpec.builder(ClassName.get("org.spongepowered.asm.mixin", "Mixin"))
                                .addMember("value", "$injectionTargetTypeMirror.class")
                                .build()
                        )
                        .addAnnotation(
                            AnnotationSpec.builder(ClassName.get("org.spongepowered.asm.mixin", "Implements"))
                                .addMember(
                                    "value", "@\$T(iface = \$T.class, prefix=\$S, remap=\$T.ONLY_PREFIXED)",
                                    ClassName.get("org.spongepowered.asm.mixin", "Interface"),
                                    ClassName.get(injectedInterfaceClassElement as TypeElement),
                                    "proxy$",
                                    ClassName.get("org.spongepowered.asm.mixin", "Interface.Remap")
                                )
                                .build()
                        )
                }

            // search for methods marked with @Inject in the class surrogate
            injectedInterfaceClassElement.enclosedElements
                .filter { it.kind == ElementKind.METHOD }
                .filter { it.getAnnotation(Inject::class.java) != null }
                .filterIsInstance<ExecutableElement>()
                .forEach { methodElement ->
                    injectMethod(
                        methodElement,
                        semanticVersion,
                        mixinClassBuilder,
                        injectionTargetTypeElement.kind == ElementKind.INTERFACE
                    )
                }

            JavaFile.builder(GEN_MIXIN_PACKAGE_NAME, mixinClassBuilder.build()).build().writeTo(processingEnv.filer)
            generatedClasses += mixinClassName
        }

        // generate automixins json file
        processingEnv.filer.createResource(
            StandardLocation.CLASS_OUTPUT,
            "",
            dummy.getAnnotation(GenerateInjections::class.java).metaFileName
        )
            .openWriter().use { writer ->
                writer.write(
                    """
                    {
                      "required": true,
                      "package": $GEN_MIXIN_PACKAGE_NAME,
                      "refmap": "net.cydhra.refmap.json",
                      "compatibilityLevel": "JAVA_8",
                      "client": [ ${generatedClasses.joinToString(",") { "\"$it\"" }} ]
                    }
                """.trimIndent()
                )
            }
    }

    /**
     * Generate a method mixin into the given [mixinClassBuilder] for a given [methodElement]
     *
     * @param methodElement the method of the injected api interface that is added as a mixin method
     * @param semanticVersion semantic minecraft version injections are generated for
     * @param mixinClassBuilder class builder where to add the method
     * @param isInterface whether the injection target is an interface
     */
    @Suppress("DuplicatedCode")
    private fun injectMethod(
        methodElement: ExecutableElement,
        semanticVersion: Semver,
        mixinClassBuilder: TypeSpec.Builder,
        isInterface: Boolean
    ) {
        val injectionDefinition = methodElement.getAnnotation(Inject::class.java)

        val injectionTarget = try {
            selectValueByVersionConstraint(
                injectionDefinition.values,
                injectionDefinition.versions,
                semanticVersion
            )
        } catch (e: IllegalStateException) {
            processingEnv.messager.printMessage(
                Diagnostic.Kind.ERROR,
                "${methodElement.simpleName} has no version constraint matching ${semanticVersion.originalValue}"
            )
            return
        } catch (e: IllegalArgumentException) {
            processingEnv.messager.printMessage(
                Diagnostic.Kind.ERROR,
                "${methodElement.simpleName} has no injection target reference for " +
                        "version constraint '${injectionDefinition.versions[0]}'"
            )
            return
        }

        val injectionType = selectInjectionTypeByVersionConstraint(
            injectionDefinition.types,
            injectionDefinition.versions,
            semanticVersion
        )

        when (injectionType) {
            InjectionType.FIELD -> {
                if (methodElement.parameters.isNotEmpty()) {
                    if (methodElement.parameters.size > 1 || !methodElement.simpleName.startsWith("set")) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.simpleName} is injected by field but defines too many" +
                                    " parameters or is not a setter"
                        )
                        return
                    }

                    try {
                        generateSetter(methodElement, mixinClassBuilder, injectionTarget, semanticVersion)
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.parameters[0].asType()} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }
                } else {
                    try {
                        generateShadowProperty(
                            mixinClassBuilder,
                            mapToRuntimeType(methodElement.returnType, semanticVersion),
                            injectionTarget
                        )
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.returnType} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }

                    generateGetterMethod(methodElement, mixinClassBuilder, injectionTarget)
                }
            }
            InjectionType.METHOD -> {
                val shadowMethodBuilder =
                    try {
                        MethodSpec.methodBuilder(injectionTarget)
                            .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                            .returns(TypeName.get(mapToRuntimeType(methodElement.returnType, semanticVersion)))
                            .addAnnotation(ClassName.get("org.spongepowered.asm.mixin", "Shadow"))
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.returnType} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }
                val injectedMethodBuilder =
                    MethodSpec.methodBuilder(methodElement.simpleName.toString())
                        .addModifiers(Modifier.PUBLIC)
                        .returns(TypeName.get(methodElement.returnType))

                methodElement.parameters.forEach { parameter ->
                    injectedMethodBuilder.addParameter(ParameterSpec.get(parameter))

                    try {
                        shadowMethodBuilder
                            .addParameter(
                                ParameterSpec.builder(
                                    TypeName.get(
                                        mapToRuntimeType(
                                            parameter.asType(),
                                            semanticVersion
                                        )
                                    ),
                                    parameter.simpleName.toString(),
                                    *parameter.modifiers.toTypedArray()
                                ).build()
                            )
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${parameter.asType()} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }
                }

                injectedMethodBuilder.addCode(generateProxyMethodCode(methodElement, injectionTarget, semanticVersion))

                if (isInterface) {
                    injectedMethodBuilder.addModifiers(Modifier.DEFAULT)
                }

                mixinClassBuilder.addMethod(shadowMethodBuilder.build())
                mixinClassBuilder.addMethod(injectedMethodBuilder.build())
            }
            InjectionType.DELEGATION -> {
                val injectionShadowTypeAndDelegate = injectionTarget.split(" ");

                val injectionShadowTarget = if (injectionShadowTypeAndDelegate.size == 2) {
                    injectionShadowTypeAndDelegate[1]
                } else {
                    injectionShadowTypeAndDelegate[0]
                }

                if (methodElement.parameters.isNotEmpty()) {
                    if (methodElement.parameters.size > 1 || !methodElement.simpleName.startsWith("set")) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.simpleName} is injected by field but defines too many" +
                                    " parameters or is not a setter"
                        )
                        return
                    }

                    try {
                        generateSetter(methodElement, mixinClassBuilder, injectionShadowTarget, semanticVersion)
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.parameters[0].asType()} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }
                } else {
                    // only add a shadow property if the type is given. Otherwise assume, that the shadow property already exists
                    if (injectionShadowTypeAndDelegate.size == 2) {
                        val injectionShadowType = injectionShadowTypeAndDelegate[0]
                        val injectionShadowName = injectionShadowTypeAndDelegate[1].split(".")[0]

                        try {
                            generateShadowProperty(
                                mixinClassBuilder,
                                processingEnv.elementUtils.getTypeElement(injectionShadowType).asType(),
                                injectionShadowName
                            )
                        } catch (e: IllegalArgumentException) {
                            processingEnv.messager.printMessage(
                                Diagnostic.Kind.ERROR,
                                "${methodElement.returnType} points to a non-existent platform type ${e.message}"
                            )
                            return
                        }
                    }

                    generateGetterMethod(methodElement, mixinClassBuilder, injectionShadowTarget)
                }
            }
            InjectionType.INTRINSIC -> {
                val shadowMethodBuilder =
                    try {
                        MethodSpec.methodBuilder(methodElement.simpleName.toString())
                            .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                            .returns(TypeName.get(mapToRuntimeType(methodElement.returnType, semanticVersion)))
                            .addAnnotation(ClassName.get("org.spongepowered.asm.mixin", "Shadow"))
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${methodElement.returnType} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }
                val injectedMethodBuilder =
                    MethodSpec.methodBuilder("proxy\$${methodElement.simpleName}")
                        .addModifiers(Modifier.PUBLIC)
                        .addAnnotation(
                            AnnotationSpec.builder(
                                ClassName.get("org.spongepowered.asm.mixin", "Intrinsic")
                            )
                                .addMember("displace", "true")
                                .build()
                        )
                        .returns(TypeName.get(methodElement.returnType))

                methodElement.parameters.forEach { parameter ->
                    injectedMethodBuilder.addParameter(ParameterSpec.get(parameter))

                    try {
                        shadowMethodBuilder
                            .addParameter(
                                ParameterSpec.builder(
                                    TypeName.get(
                                        mapToRuntimeType(
                                            parameter.asType(),
                                            semanticVersion
                                        )
                                    ),
                                    parameter.simpleName.toString(),
                                    *parameter.modifiers.toTypedArray()
                                ).build()
                            )
                    } catch (e: IllegalArgumentException) {
                        processingEnv.messager.printMessage(
                            Diagnostic.Kind.ERROR,
                            "${parameter.asType()} points to a non-existent platform type ${e.message}"
                        )
                        return
                    }
                }

                injectedMethodBuilder.addCode(
                    generateProxyMethodCode(
                        methodElement, methodElement.simpleName.toString(),
                        semanticVersion
                    )
                )

                if (isInterface) {
                    injectedMethodBuilder.addModifiers(Modifier.DEFAULT)
                }

                mixinClassBuilder.addMethod(shadowMethodBuilder.build())
                mixinClassBuilder.addMethod(injectedMethodBuilder.build())
            }
        }
    }

    private fun selectInjectionTypeByVersionConstraint(
        injectionTypes: Array<out InjectionType>,
        versionConstraints: Array<out String>,
        semanticVersion: Semver
    ): InjectionType {
        // if only one injection type is specified, take it
        if (injectionTypes.size == 1) {
            return injectionTypes[0]
        }

        // find the correct injection target by version
        val versionIndex = versionConstraints
            .withIndex()
            .reversed()
            .firstOrNull { (_, version) -> semanticVersion.satisfies(version) }
            ?.index ?: throw IllegalStateException()

        return try {
            injectionTypes[versionIndex]
        } catch (e: ArrayIndexOutOfBoundsException) {
            throw IllegalArgumentException()
        }
    }

    /**
     * Generate and add a setter method for a specific shadow field into the mixin class.
     *
     * @param methodElement the method element of the infra interface that is generated
     * @param mixinClassBuilder the class builder for the generated mixin class
     * @param injectionShadowTarget the name of the shadow field that is accessed by the setter
     * @param semanticVersion the semantic version the method is generated for
     */
    private fun generateSetter(
        methodElement: ExecutableElement,
        mixinClassBuilder: TypeSpec.Builder,
        injectionShadowTarget: String,
        semanticVersion: Semver
    ) {
        mixinClassBuilder.addMethod(
            MethodSpec.methodBuilder(methodElement.simpleName.toString())
                .addModifiers(Modifier.PUBLIC)
                .addParameter(ParameterSpec.get(methodElement.parameters[0]))
                .addStatement(
                    "$injectionShadowTarget = (\$T) \$N;",
                    mapToRuntimeType(methodElement.parameters[0].asType(), semanticVersion),
                    methodElement.parameters[0].simpleName
                )
                .build()
        )
    }

    /**
     * Generate and add a shadow field to the mixin class, that is used to access an underlying field in the platform type.
     *
     * @param mixinClassBuilder the class builder for the generated mixin
     * @param fieldType the type of the shadow property
     * @param injectionShadowTarget the name of the shadow property
     */
    private fun generateShadowProperty(
        mixinClassBuilder: TypeSpec.Builder,
        fieldType: TypeMirror,
        injectionShadowTarget: String
    ) {
        mixinClassBuilder.addField(
            FieldSpec.builder(
                TypeName.get(fieldType),
                injectionShadowTarget
            )
                .addAnnotation(ClassName.get("org.spongepowered.asm.mixin", "Shadow"))
                .addAnnotation(JvmField::class.java)
                .build()
        )
    }

    /**
     * Generate and add a getter method into the generated mixin
     *
     * @param methodElement the method of the infra interface that is implemented through this getter
     * @param mixinClassBuilder the class builder for the generated mixin
     * @param injectionShadowTarget the name of the shadow property that is being accessed by the getter
     */
    private fun generateGetterMethod(
        methodElement: ExecutableElement,
        mixinClassBuilder: TypeSpec.Builder,
        injectionShadowTarget: String
    ) {
        mixinClassBuilder.addMethod(
            MethodSpec.methodBuilder(methodElement.simpleName.toString())
                .addModifiers(Modifier.PUBLIC)
                .addStatement("return (\$T) \$N", methodElement.returnType, injectionShadowTarget)
                .returns(TypeName.get(methodElement.returnType))
                .build()
        )
    }
}