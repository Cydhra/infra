package infra.processors.injection

import kotlin.reflect.KClass

/**
 * An annotation that must be present in a version module. It lists every core API interface, that gets injected into
 * the platform client and triggers the respective annotation processing for those interfaces.
 *
 * @param value all classes to generate injections for
 * @param metaFileName name of the file where to list the mixins for the launch wrapper
 */
@Repeatable
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class GenerateInjections(
    vararg val value: KClass<out Any>,
    val metaFileName: String = "automixins.infra.json"
)