package infra.processors.injection

/**
 * Different methods of member injection.
 */
enum class InjectionType {
    /**
     * Delegate get and set calls to a field
     */
    FIELD,

    /**
     * Delegate class to a method
     */
    METHOD,

    /**
     * Delegate get and set calls to a member in the platform type
     */
    DELEGATION,

    /**
     * Generate an intrinsic proxy that displaces the original method
     */
    INTRINSIC,
}