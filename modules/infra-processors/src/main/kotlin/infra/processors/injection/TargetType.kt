package infra.processors.injection

/**
 * Types from the core modules are annotated with this annotation. It defines a mapping from the API types to
 * Minecraft platform types. A platform type specified by this annotation will implement the API type by injection
 * and calls from and to this platform type, as well as calls involving parameters of this platform type will be cast
 * from and to the API type.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class TargetType(
    /**
     * The fully qualified paths to the respective platform type in each platform version. The type is selected by a
     * semantic version check.
     *
     * @see versions
     */
    vararg val values: String,

    /**
     * A set of semantic version expressions. The semantic version of the platform that is currently compiled against
     * will be checked against the expressions found in this array from back to front. The first matching version
     * expression will decide the value used from the [values] array. It will be chosen by matching the index.
     * Therefore, for each value in the array there must be a matching version expression.
     *
     * @see values
     */
    val versions: Array<String> = ["*"]
)