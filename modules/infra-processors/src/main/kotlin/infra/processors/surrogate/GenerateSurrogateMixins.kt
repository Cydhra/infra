package infra.processors.surrogate

import kotlin.reflect.KClass

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class GenerateSurrogateMixins(
    vararg val values: KClass<out Any>,
    val mixinMetaFileName: String = "autosurrogates.infra.json"
)