package infra.processors.surrogate

/**
 * Annotate API interfaces that have sub- or superinterfaces with a [PlatformSurrogate] annotation, to generate
 * surrogate unpacking code in methods accepting this API interface
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Surrogated