package infra.processors.surrogate

import kotlin.reflect.KClass

/**
 * A surrogate type used by the core module that proxies calls to a platform type later implemented by a version
 * module. The surrogate is automatically implemented by the annotation processor within the core module and simply
 * implements the target interface as a decorator. Within the version module a mixin is generated that populates the
 * surrogate target from the surrogate constructor.
 */
@Retention(AnnotationRetention.RUNTIME)
annotation class PlatformSurrogate(val value: KClass<out Any>)