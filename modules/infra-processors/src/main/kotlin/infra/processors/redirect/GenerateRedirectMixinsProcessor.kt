package infra.processors.redirect

import com.squareup.javapoet.*
import com.vdurmont.semver4j.Semver
import infra.processors.AbstractMixinGeneratorProcessor
import javax.annotation.processing.SupportedAnnotationTypes
import javax.annotation.processing.SupportedOptions
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.Modifier
import javax.lang.model.type.MirroredTypesException
import javax.lang.model.type.NoType
import javax.lang.model.type.TypeMirror
import javax.tools.StandardLocation

@SupportedAnnotationTypes("infra.processors.redirect.GenerateRedirects")
@SupportedOptions("mc_version")
class GenerateRedirectMixinsProcessor : AbstractMixinGeneratorProcessor(GenerateRedirects::class) {

    companion object {
        private const val GEN_MIXIN_PACKAGE_NAME = "infra.redirects"
    }

    override fun generateMixins(dummy: Element, semanticVersion: Semver) {
        val typeElements = try {
            dummy.getAnnotation(GenerateRedirects::class.java).values
            throw AssertionError("the previous call already must fail")
        } catch (e: MirroredTypesException) {
            e.typeMirrors.map(processingEnv.typeUtils::asElement)
        }

        val generatedClasses = mutableListOf<String>()

        typeElements.forEach { redirectObject ->
            val mixinClassName = redirectObject.simpleName.toString().removePrefix("Infra")
            val targetType = mapToRuntimeType(redirectObject.asType(), semanticVersion)

            val mixinClassBuilder = TypeSpec.classBuilder(mixinClassName)
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .addAnnotation(
                    AnnotationSpec.builder(ClassName.get("org.spongepowered.asm.mixin", "Mixin"))
                        .addMember("value", "$redirectObject.class")
                        .build()
                )

            redirectObject.enclosedElements
                .filterIsInstance<ExecutableElement>()
                .filter { it.kind == ElementKind.METHOD }
                .filter { it.getAnnotation(Redirect::class.java) != null }
                .forEachIndexed { index, method ->
                    injectRedirectHandler(targetType, method, mixinClassBuilder, semanticVersion)
                }

            JavaFile.builder(GEN_MIXIN_PACKAGE_NAME, mixinClassBuilder.build()).build().writeTo(processingEnv.filer)
            generatedClasses += mixinClassName

        }

        processingEnv.filer.createResource(
            StandardLocation.CLASS_OUTPUT,
            "",
            dummy.getAnnotation(GenerateRedirects::class.java).mixinMetaFileName
        )
            .openWriter().use { writer ->
                writer.write(
                    """
                    {
                      "required": true,
                      "package": $GEN_MIXIN_PACKAGE_NAME,
                      "refmap": "net.cydhra.refmap.json",
                      "compatibilityLevel": "JAVA_8",
                      "client": [ ${generatedClasses.joinToString(",") { "\"$it\"" }} ]
                    }
                """.trimIndent()
                )
            }
    }

    /**
     * Inject a handler for a redirect into the given mixin class builder. It will redirect calls to the given method
     * to a new target in the platform
     *
     * @param targetType the platform class that contains the target of redirection
     * @param method the method that shall be redirected
     * @param mixinClassBuilder the mixin that is being generated to handle redirection injection
     * @param semanticVersion the semantic minecraft version
     */
    fun injectRedirectHandler(
        targetType: TypeMirror,
        method: ExecutableElement,
        mixinClassBuilder: TypeSpec.Builder,
        semanticVersion: Semver
    ) {
        val redirectMethodHandlerBuilder = MethodSpec.methodBuilder("${method.simpleName}Redirect")
            .addAnnotation(
                AnnotationSpec
                    .builder(ClassName.get("org.spongepowered.asm.mixin.injection", "Inject"))
                    .addMember(
                        "method", "\"${method.simpleName}${generateByteCodeDescriptor(
                            *method.parameters.map(Element::asType)
                                .toTypedArray()
                        )}\""
                    )
                    .addMember("at", "@org.spongepowered.asm.mixin.injection.At(\"HEAD\")")
                    .addMember("cancellable", "true")
                    .build()
            )
            .addModifiers(Modifier.PUBLIC)

        val redirectionTargetName = selectValueByVersionConstraint(
            method.getAnnotation(Redirect::class.java).values,
            method.getAnnotation(Redirect::class.java).versions,
            semanticVersion
        )

        val redirectionType = selectValueByVersionConstraint(
            method.getAnnotation(Redirect::class.java).types,
            method.getAnnotation(Redirect::class.java).versions,
            semanticVersion
        )

        // add method parameters
        method.parameters.forEach { param ->
            redirectMethodHandlerBuilder.addParameter(ParameterSpec.get(param))
        }

        redirectMethodHandlerBuilder.addParameter(
            ParameterSpec.builder(
                if (method.returnType is NoType) {
                    TypeName.get(
                        processingEnv.elementUtils.getTypeElement(
                            "org.spongepowered.asm.mixin.injection.callback.CallbackInfo"
                        ).asType()
                    )
                } else {
                    ParameterizedTypeName.get(
                        ClassName.get("org.spongepowered.asm.mixin.injection.callback", "CallbackInfoReturnable"),
                        TypeName.get(method.returnType).box()
                    )
                },
                "info"
            ).build()
        )

        when (redirectionType) {
            RedirectType.METHOD -> {
                redirectMethodHandlerBuilder.addCode(
                    generateProxyMethodCode(
                        method,
                        "${ClassName.get(targetType)}.$redirectionTargetName",
                        semanticVersion,
                        callback = true
                    )
                )
            }
            RedirectType.FIELD -> {
                var codeBlock = generateSurrogateUnpackingCode(method, CodeBlock.of(""))

                if (method.parameters.size == 1) {
                    codeBlock = CodeBlock.join(
                        listOf(
                            codeBlock,
                            CodeBlock.of(
                                "\$T.$redirectionTargetName = (\$T) ${method.parameters[0].simpleName};",
                                targetType,
                                mapToRuntimeElement(method.parameters[0].asType(), semanticVersion).asType()
                            )
                        ),
                        "\n"
                    )
                } else if (method.parameters.size == 0) {
                    codeBlock = CodeBlock.join(
                        listOf(
                            codeBlock,
                            CodeBlock.of(
                                "info.setReturnValue((\$T) \$T.$redirectionTargetName);",
                                method.returnType,
                                targetType
                            )
                        ),
                        "\n"
                    )
                } else {
                    throw IllegalStateException("cannot redirect methods with more than one argument to fields")
                }

                redirectMethodHandlerBuilder.addCode(codeBlock)
            }
            RedirectType.CODE -> {
                var codeBlock = generateSurrogateUnpackingCode(method, CodeBlock.of(""))

                codeBlock = if (method.returnType is NoType) {
                    CodeBlock.join(
                        listOf(
                            codeBlock,
                            CodeBlock.of("$redirectionTargetName;")
                        ), ""
                    )
                } else {
                    CodeBlock.join(
                        listOf(
                            codeBlock,
                            CodeBlock.of("info.setReturnValue($redirectionTargetName);")
                        ), ""
                    )
                }

                redirectMethodHandlerBuilder.addCode(codeBlock)
            }
        }

        mixinClassBuilder.addMethod(redirectMethodHandlerBuilder.build())
    }
}