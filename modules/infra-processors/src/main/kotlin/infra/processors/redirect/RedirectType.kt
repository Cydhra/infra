package infra.processors.redirect

enum class RedirectType {
    METHOD,
    FIELD,
    CODE
}