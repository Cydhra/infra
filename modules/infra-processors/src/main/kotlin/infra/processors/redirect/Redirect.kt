package infra.processors.redirect

import infra.processors.redirect.RedirectType.METHOD

/**
 * Methods within the core modules are annotated with this annotation to inject static calls into the platform. This
 * annotation is therefore not used to annotate members of interfaces, but rather non-abstract methods called
 * directly from within the core modules.
 *
 * The methods annotated with this annotation should not contain code. However, if the annotated method does not
 * return any value, code residing in the original method body will be executed as well. Methods that do return a
 * value, will not execute code, that is implemented in their body within the core module.
 *
 * To ensure that an annotated method will compile successfully, if it is ought to return a value, call
 * [RedirectedControlFlow] in its body.
 *
 * ## Example
 * ```
 * @Redirect("isRunningOnMac", versions=["1.8.9"], types=[RedirectType.FIELD])
 * fun isRunningOnMac(): Boolean {
 *      RedirectedControlFlow()
 * }
 * ```
 */
@Retention(AnnotationRetention.RUNTIME)
annotation class Redirect(
    /**
     * The runtime member names that shall be statically called. Depending on the [RedirectType] those members can be
     * methods or fields of the [infra.processors.injection.TargetType] defined by the enclosing member of the
     * annotated method, or code.
     *
     * The member name chosen depends on the matching version.
     *
     * @see [versions]
     */
    vararg val values: String,

    /**
     * A set of semantic version expressions. The semantic version of the platform that is currently compiled against
     * will be checked against the expressions found in this array from back to front. The first matching version
     * expression will decide the value used from the [values] array. It will be chosen by matching the index.
     * Therefore, for each value in the array there must be a matching version expression.
     *
     * @see values
     */
    val versions: Array<String> = ["*"],

    /**
     * The redirection type indicates where to redirect the method call. It can be redirected to static method calls,
     * static fields or code access.
     *
     * The member name chosen depends on the matching version.
     *
     * @see versions
     * @see RedirectType
     */
    val types: Array<RedirectType> = [METHOD]
)