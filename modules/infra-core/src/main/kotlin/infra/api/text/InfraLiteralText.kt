package infra.api.text

import infra.processors.injection.TargetType
import infra.processors.surrogate.PlatformSurrogate

@TargetType(
    "net.minecraft.util.ChatComponentText",
    "net.minecraft.text.LiteralText",
    versions = ["1.8.9", "^1.14.4"]
)
@PlatformSurrogate(LiteralTextSurrogate::class)
interface InfraLiteralText : InfraBaseText {

}