package infra.api.text

import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.INTRINSIC
import infra.processors.injection.InjectionType.METHOD
import infra.processors.injection.TargetType
import infra.processors.surrogate.Surrogated

@TargetType(
    "net.minecraft.util.IChatComponent",
    "net.minecraft.text.Text",
    versions = ["^1.8.9", "^1.14.4"]
)
@Surrogated
interface InfraText {

    @Inject("getUnformattedText", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun asString(): String

    @Inject("appendText", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun append(text: String): InfraText

    @Inject("appendSibling", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun append(text: InfraText): InfraText
}