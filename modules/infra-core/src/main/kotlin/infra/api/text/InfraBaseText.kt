package infra.api.text

import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.INTRINSIC
import infra.processors.injection.TargetType
import infra.processors.surrogate.Surrogated

@TargetType(
    "net.minecraft.util.ChatComponentStyle",
    "net.minecraft.text.BaseText",
    versions = ["1.8.9", "^1.14.4"]
)
@Surrogated
interface InfraBaseText : InfraText {
    val siblings: List<InfraText>
        @Inject("", types = [INTRINSIC]) get
}