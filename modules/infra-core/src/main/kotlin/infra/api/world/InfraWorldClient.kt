package infra.api.world

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.multiplayer.WorldClient",
    "net.minecraft.client.world.ClientWorld",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraWorldClient {

}