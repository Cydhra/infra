package infra.api.util.math

import infra.processors.injection.TargetType
import infra.processors.surrogate.PlatformSurrogate

@TargetType(
    "net.minecraft.util.BlockPos",
    "net.minecraft.util.math.BlockPos",
    versions = ["^1.8.9", "^1.14.4"]
)
@PlatformSurrogate(BlockPosSurrogate::class)
interface InfraBlockPos {

}