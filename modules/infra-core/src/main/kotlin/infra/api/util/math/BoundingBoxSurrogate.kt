package infra.api.util.math

/**
 * Surrogate constructor template for AxisAlignedBB.
 */
class BoundingBoxSurrogate private constructor(x1: Double, y1: Double, z1: Double, x2: Double, y2: Double, z2: Double) {

    private constructor(pos1: InfraBlockPos, pos2: InfraBlockPos) : this(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
}