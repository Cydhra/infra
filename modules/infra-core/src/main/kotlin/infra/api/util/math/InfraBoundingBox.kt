package infra.api.util.math

import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.*
import infra.processors.injection.TargetType
import infra.processors.surrogate.PlatformSurrogate

@TargetType(
    "net.minecraft.util.AxisAlignedBB",
    "net.minecraft.util.math.Box",
    versions = ["^1.8.9", "^1.14.4"]
)
@PlatformSurrogate(BoundingBoxSurrogate::class)
interface InfraBoundingBox {

    val minX: Double
        @Inject("minX", types = [FIELD]) get

    val minY: Double
        @Inject("minY", types = [FIELD]) get

    val minZ: Double
        @Inject("minZ", types = [FIELD]) get

    val maxX: Double
        @Inject("maxX", types = [FIELD]) get

    val maxY: Double
        @Inject("maxY", types = [FIELD]) get

    val maxZ: Double
        @Inject("maxZ", types = [FIELD]) get

    @Inject("", types = [INTRINSIC])
    fun expand(x: Double, y: Double, z: Double): InfraBoundingBox

    @Inject("", types = [INTRINSIC])
    fun union(other: InfraBoundingBox): InfraBoundingBox

    @Inject("", types = [INTRINSIC])
    fun offset(x: Double, y: Double, z: Double): InfraBoundingBox

    @Inject("intersectsWith", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun intersects(other: InfraBoundingBox): Boolean


}