package infra.api.client.gui

import infra.processors.redirect.Redirect
import infra.processors.redirect.RedirectType.CODE
import infra.processors.redirect.RedirectedControlFlow

object InfraWindow {

    @Redirect(
        "new net.minecraft.client.gui.ScaledResolution(net.minecraft.client.Minecraft.getMinecraft())" +
                ".getScaledWidth()",
        "net.minecraft.client.MinecraftClient.getInstance().window.getScaledWidth()",
        versions = ["^1.8.9", "^1.14.4"],
        types = [CODE, CODE]
    )
    fun getScaledWidth(): Int = RedirectedControlFlow()

    @Redirect(
        "new net.minecraft.client.gui.ScaledResolution(net.minecraft.client.Minecraft.getMinecraft())" +
                ".getScaledHeight()",
        "net.minecraft.client.MinecraftClient.getInstance().window.getScaledHeight()",
        versions = ["^1.8.9", "^1.14.4"],
        types = [CODE, CODE]
    )
    fun getScaledHeight(): Int = RedirectedControlFlow()

    fun getScaledResolution(): InfraScaledResolution {
        return InfraScaledResolution(this.getScaledWidth(), this.getScaledHeight())
    }

    /**
     * A convenience wrapper to get the scaled resolution of the Minecraft window.
     */
    data class InfraScaledResolution(val scaledWidth: Int, val scaledHeight: Int)
}