package infra.api.client.gui

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.gui.GuiScreen",
    "net.minecraft.client.gui.screen.Screen",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraGuiScreen {
}