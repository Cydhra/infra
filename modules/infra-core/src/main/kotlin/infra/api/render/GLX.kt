package infra.api.render

import infra.processors.injection.TargetType
import infra.processors.redirect.Redirect
import infra.processors.redirect.RedirectType.METHOD
import infra.processors.redirect.RedirectedControlFlow

@TargetType(
    "net.minecraft.client.renderer.OpenGlHelper",
    "com.mojang.blaze3d.platform.GLX",
    versions = ["^1.8.9", "^1.14.4"]
)
object GLX {
    @Redirect("initializeTextures", "init", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun init() {
    }

    @Redirect("glAttachShader")
    fun attachShader(program: Int, shader: Int) {
    }

    @Redirect("glDeleteShader")
    fun deleteShader(shader: Int) {
    }

    @Redirect("glCreateShader")
    fun createShader(shaderType: Int) {
    }

    @Redirect("glCompileShader")
    fun compileShader(shader: Int) {
    }

    @Redirect("glCreateProgram")
    fun createProgram(): Int {
        RedirectedControlFlow()
    }

    @Redirect("glUseProgram")
    fun useProgram(program: Int) {
    }

    @Redirect("glDeleteProgram")
    fun deleteProgram(program: Int) {
    }

    @Redirect("glLinkProgram")
    fun linkProgram(program: Int) {
    }

    @Redirect("glGetUniformLocation")
    fun getUniformLocation(program: Int, name: CharSequence): Int {
        RedirectedControlFlow()
    }

    @Redirect("glGetAttribLocation")
    fun getAttribLocation(program: Int, name: CharSequence): Int {
        RedirectedControlFlow()
    }

    @Redirect("glGenBuffers")
    fun genBuffers(): Int {
        RedirectedControlFlow()
    }

    @Redirect("glBindBuffer")
    fun bindBuffer(target: Int, buffer: Int) {
    }

    @Redirect("glDeleteBuffers")
    fun deleteBuffers(buffer: Int) {
    }

    @Redirect("glBindFramebuffer")
    fun bindFrameBuffer(target: Int, frameBuffer: Int) {
    }

    @Redirect("glBindRenderbuffer")
    fun bindRenderBuffer(target: Int, renderVuffer: Int) {
    }

    @Redirect("glDeleteRenderbuffers")
    fun deleteRenderBuffers(renderBuffer: Int) {
    }

    @Redirect("glDeleteFramebuffers")
    fun deleteFrameBuffers(frameBuffer: Int) {
    }

    @Redirect("glGenFramebuffers")
    fun genFrameBuffers(): Int {
        RedirectedControlFlow()
    }

    @Redirect("glGenRenderbuffers")
    fun genRenderBuffers(): Int {
        RedirectedControlFlow()
    }

    @Redirect("glRenderbufferStorage")
    fun renderBufferStorage(target: Int, internalFormat: Int, width: Int, height: Int) {
    }

    @Redirect("glFramebufferRenderbuffer")
    fun frameBufferRenderBuffer(target: Int, attachment: Int, renderbufferTarget: Int, renderbuffer: Int) {
    }
}