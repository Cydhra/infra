package infra.api.render.shader

import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.*
import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.shader.Framebuffer",
    "net.minecraft.client.gl.GlFramebuffer",
    versions = ["^1.8.9", "^1.14.4"]
)
interface InfraFramebuffer {
    var width: Int
        @Inject("framebufferWidth", "viewWidth", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD]) get
        @Inject("framebufferWidth", "viewWidth", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD]) set

    var height: Int
        @Inject("framebufferHeight", "viewHeight", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD]) get
        @Inject("framebufferHeight", "viewHeight", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD]) set


    var depthAttachment: Int
        @Inject("depthBuffer", "depthAttachment", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD]) get
        @Inject("depthBuffer", "depthAttachment", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD]) set

    @Inject("bindFramebuffer", "beginWrite", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun bind(setViewport: Boolean)

    @Inject("unbindFramebuffer", "endWrite", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun unbind()

    @Inject("deleteFramebuffer", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun delete()
}