package infra.api.render.tessellator

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.renderer.vertex.VertexFormat",
    "net.minecraft.client.render.VertexFormat",
    versions = ["^1.8.9", "^1.14.4"]
)
interface InfraVertexFormat