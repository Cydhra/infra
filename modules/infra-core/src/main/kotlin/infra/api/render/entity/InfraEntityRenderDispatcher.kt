package infra.api.render.entity

import infra.api.entity.InfraEntity
import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.METHOD
import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.renderer.entity.RenderManager",
    "net.minecraft.client.render.entity.EntityRenderDispatcher",
    versions = ["^1.8.9", "^1.14.4"]
)
interface InfraEntityRenderDispatcher {

    @Inject("renderEntityStatic", "render", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun renderEntity(entity: InfraEntity, partialTicks: Float, hideDebugBox: Boolean): Boolean
}