package infra.api.render.camera

import infra.api.util.math.InfraBoundingBox
import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.INTRINSIC
import infra.processors.injection.InjectionType.METHOD
import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.renderer.culling.Frustum",
    "net.minecraft.client.render.FrustumWithOrigin",
    versions = ["^1.8.9", "^1.14.4"]
)
interface InfraFrustum {

    @Inject("", "setOrigin", versions = ["^1.8.9", "^1.14.4"], types = [INTRINSIC, METHOD])
    fun setPosition(x: Double, y: Double, z: Double)

    @Inject("isBoxInFrustum", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun intersects(minX: Double, minY: Double, minZ: Double, maxX: Double, maxY: Double, maxZ: Double): Boolean

    @Inject("isBoundingBoxInFrustum", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun intersects(boundingBox: InfraBoundingBox): Boolean
}