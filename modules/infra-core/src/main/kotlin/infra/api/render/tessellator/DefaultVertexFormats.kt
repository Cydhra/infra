package infra.api.render.tessellator

import infra.processors.injection.TargetType
import infra.processors.redirect.Redirect
import infra.processors.redirect.RedirectType.FIELD
import infra.processors.redirect.RedirectedControlFlow

@TargetType(
    "net.minecraft.client.renderer.vertex.DefaultVertexFormats",
    "net.minecraft.client.render.VertexFormats",
    versions = ["^1.8.9", "^1.14.4"]
)
object DefaultVertexFormats {

    val BLOCK: InfraVertexFormat
        @Redirect("BLOCK", "POSITION_COLOR_UV_LMAP", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD])
        get() = RedirectedControlFlow()

    val ITEM: InfraVertexFormat
        @Redirect("ITEM", "POSITION_COLOR_UV_NORMAL", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD])
        get() = RedirectedControlFlow()

    val OLDMODEL_POSITION_TEX_NORMAL: InfraVertexFormat
        @Redirect(
            "OLDMODEL_POSITION_TEX_NORMAL",
            "POSITION_UV_NORMAL_2",
            versions = ["^1.8.9", "^1.14.4"],
            types = [FIELD, FIELD]
        )
        get() = RedirectedControlFlow()

    val PARTICLE_POSITION_TEX_COLOR_LMAP: InfraVertexFormat
        @Redirect(
            "PARTICLE_POSITION_TEX_COLOR_LMAP",
            "POSITION_UV_COLOR_LMAP",
            versions = ["^1.8.9", "^1.14.4"],
            types = [FIELD, FIELD]
        )
        get() = RedirectedControlFlow()

    val POSITION: InfraVertexFormat
        @Redirect("POSITION", types = [FIELD]) get() = RedirectedControlFlow()

    val POSITION_COLOR: InfraVertexFormat
        @Redirect("POSITION_COLOR", types = [FIELD]) get() = RedirectedControlFlow()

    val POSITION_TEX: InfraVertexFormat
        @Redirect("POSITION_TEX", "POSITION_UV", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD])
        get() = RedirectedControlFlow()

    val POSITION_NORMAL: InfraVertexFormat
        @Redirect("POSITION_NORMAL", types = [FIELD]) get() = RedirectedControlFlow()

    val POSITION_TEX_COLOR: InfraVertexFormat
        @Redirect("POSITION_TEX_COLOR", "POSITION_UV_COLOR", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD])
        get() = RedirectedControlFlow()

    val POSITION_TEX_NORMAL: InfraVertexFormat
        @Redirect("POSITION_TEX_NORMAL", "POSITION_UV_NORMAL", versions = ["^1.8.9", "^1.14.4"], types = [FIELD, FIELD])
        get() = RedirectedControlFlow()

    val POSITION_TEX_LMAP_COLOR: InfraVertexFormat
        @Redirect(
            "POSITION_TEX_LMAP_COLOR",
            "POSITION_UV_LMAP_COLOR",
            versions = ["^1.8.9", "^1.14.4"],
            types = [FIELD, FIELD]
        )
        get() = RedirectedControlFlow()

    val POSITION_TEX_COLOR_NORMAL: InfraVertexFormat
        @Redirect(
            "POSITION_TEX_COLOR_NORMAL",
            "POSITION_UV_COLOR_NORMAL",
            versions = ["^1.8.9", "^1.14.4"],
            types = [FIELD, FIELD]
        )
        get() = RedirectedControlFlow()
}