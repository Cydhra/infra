package infra.api.render

import infra.processors.injection.TargetType
import infra.processors.redirect.Redirect
import infra.processors.redirect.RedirectType.METHOD
import infra.processors.redirect.RedirectedControlFlow

@TargetType(
    "net.minecraft.client.renderer.GlStateManager",
    "com.mojang.blaze3d.platform.GlStateManager",
    versions = ["^1.8.9", "^1.14.4"]
)
object GlStateManager {

    @Redirect("pushAttrib", "pushLightingAttributes", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun pushAttrib() {
    }

    @Redirect("popAttrib", "popAttributes", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun popAttrib() {
    }

    @Redirect("enableAlpha", "enableAlphaTest", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun enableAlphaTest() {
    }

    @Redirect("disableAlpha", "disableAlphaTest", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun disableAlpha() {
    }

    @Redirect("alphaFunc")
    fun alphaFunc(func: Int, ref: Float) {
    }

    @Redirect("enableLighting")
    fun enableLighting() {
    }

    @Redirect("disableLighting")
    fun disableLighting() {
    }

    @Redirect("enableLight")
    fun enableLight(light: Int) {
    }

    @Redirect("disableLight")
    fun disableLight(light: Int) {
    }

    @Redirect("enableColorMaterial")
    fun enableColorMaterial() {
    }

    @Redirect("disableColorMaterial")
    fun disableColorMaterial() {
    }

    @Redirect("colorMaterial")
    fun colorMaterial(face: Int, mode: Int) {
    }

    @Redirect("disableDepth", "disableDepthTest", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun disableDepth() {
    }

    @Redirect("enableDepth", "enableDepthTest", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun enableDepth() {
    }

    @Redirect("depthFunc")
    fun depthFunc(depthFunc: Int) {
    }

    @Redirect("depthMask")
    fun depthMask(flagIn: Boolean) {
    }

    @Redirect("disableBlend")
    fun disableBlend() {
    }

    @Redirect("enableBlend")
    fun enableBlend() {
    }

    @Redirect("blendFunc")
    fun blendFunc(srcFactor: Int, dstFactor: Int) {
    }

//    @Redirect("tryBlendFuncSeparate")
//    fun tryBlendFuncSeparate(srcFactor: Int, dstFactor: Int, srcFactorAlpha: Int, dstFactorAlpha: Int) {
//    }

    @Redirect("enableFog")
    fun enableFog() {
    }

    @Redirect("disableFog")
    fun disableFog() {
    }

    @Redirect("enableCull")
    fun enableCull() {
    }

    @Redirect("disableCull")
    fun disableCull() {
    }

    @Redirect("setActiveTexture", "activeTexture", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun setActiveTexture(texture: Int) {
    }

    @Redirect("enableTexture2D", "enableTexture", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun enableTexture2D() {
    }

    @Redirect("disableTexture2D", "disableTexture", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun disableTexture2D() {
    }

    @Redirect("generateTexture", "genTexture", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun generateTexture(): Int {
        RedirectedControlFlow()
    }

    @Redirect("deleteTexture")
    fun deleteTexture(texture: Int) {
    }

    @Redirect("bindTexture")
    fun bindTexture(texture: Int) {
    }

    @Redirect("enableNormalize")
    fun enableNormalize() {
    }

    @Redirect("disableNormalize")
    fun disableNormalize() {
    }

    @Redirect("shadeModel")
    fun shadeModel(mode: Int) {
    }

    @Redirect("enableRescaleNormal")
    fun enableRescaleNormal() {
    }

    @Redirect("disableRescaleNormal")
    fun disableRescaleNormal() {
    }

    @Redirect("viewport")
    fun viewport(x: Int, y: Int, width: Int, height: Int) {
    }

    @Redirect("colorMask")
    fun colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean) {
    }

    @Redirect("clearDepth")
    fun clearDepth(depth: Double) {
    }

    @Redirect("clearColor")
    fun clearColor(red: Float, green: Float, blue: Float, alpha: Float) {
    }

    @Redirect("matrixMode")
    fun matrixMode(mode: Int) {
    }

    @Redirect("loadIdentity")
    fun loadIdentity() {
    }

    @Redirect("pushMatrix")
    fun pushMatrix() {
    }

    @Redirect("popMatrix")
    fun popMatrix() {
    }

    @Redirect("ortho")
    fun ortho(left: Double, right: Double, bottom: Double, top: Double, zNear: Double, zFar: Double) {
    }

    @Redirect("rotate", "rotatef", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun rotate(angle: Float, x: Float, y: Float, z: Float) {
    }

    @Redirect("scale", "scaled", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun scale(x: Double, y: Double, z: Double) {
    }

    @Redirect("translate", "translatef", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun translate(x: Float, y: Float, z: Float) {
    }

    @Redirect("translate", "translated", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun translate(x: Double, y: Double, z: Double) {
    }

    @Redirect("color", "color4f", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun color(colorRed: Float, colorGreen: Float, colorBlue: Float, colorAlpha: Float) {
    }

    @Redirect("color", "color3f", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun color(colorRed: Float, colorGreen: Float, colorBlue: Float) {
    }

    @Redirect("callList")
    fun callList(list: Int) {
    }
}