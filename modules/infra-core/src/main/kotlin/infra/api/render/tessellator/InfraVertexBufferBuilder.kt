package infra.api.render.tessellator

import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.INTRINSIC
import infra.processors.injection.InjectionType.METHOD
import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.renderer.WorldRenderer",
    "net.minecraft.client.render.BufferBuilder",
    versions = ["^1.8.9", "^1.14.4"]
)
interface InfraVertexBufferBuilder {

    @Inject("begin", types = [METHOD])
    fun begin(glMode: Int, vertexFormat: InfraVertexFormat)

    @Inject("reset", "", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, INTRINSIC])
    fun clear()

    @Inject("", "texture", versions = ["^1.8.9", "^1.14.4"], types = [INTRINSIC, METHOD])
    fun tex(u: Double, v: Double): InfraVertexBufferBuilder

    @Inject("lightmap", "texture", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD])
    fun tex(u: Int, v: Int): InfraVertexBufferBuilder

    @Inject("", "postPosition", versions = ["^1.8.9", "^1.14.4"], types = [INTRINSIC, METHOD])
    fun putPosition(x: Double, y: Double, z: Double)

    @Inject("", types = [INTRINSIC])
    fun color(red: Int, green: Int, blue: Int, alpha: Int): InfraVertexBufferBuilder

    @Inject("", "vertex", versions = ["^1.8.9", "^1.14.4"], types = [INTRINSIC, METHOD])
    fun pos(x: Double, y: Double, z: Double): InfraVertexBufferBuilder

    @Inject("", "next", versions = ["^1.8.9", "^1.14.4"], types = [INTRINSIC, METHOD])
    fun endVertex()
}