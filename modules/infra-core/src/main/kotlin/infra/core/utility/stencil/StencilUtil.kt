package infra.core.utility.stencil

import infra.api.render.GLX
import infra.api.render.shader.InfraFramebuffer
import infra.core.utility.stencil.StencilUtil.StencilMode.CROP_INSIDE
import infra.core.utility.stencil.StencilUtil.StencilMode.CROP_OUTSIDE
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL30


object StencilUtil {

    /**
     * Enable the stencil buffer filling. Everything drawn from now on until a call of [enableStencil] will be drawn
     * onto the stencil buffer and will work as a stencil on every render call after [enableStencil]
     *
     * @param framebuffer A framebuffer object that shall be prepared for stencil operations
     */
    fun setupStencil(framebuffer: InfraFramebuffer) {
        if (framebuffer.depthAttachment > -1) {
            // disable the depth buffer as the stencil buffer will handle depth data
            GLX.deleteRenderBuffers(framebuffer.depthAttachment)
            framebuffer.depthAttachment = -1
        }

        // generate and bind a new render buffer
        val stencilBuffer: Int = GLX.genRenderBuffers()
        GLX.bindRenderBuffer(GL30.GL_RENDERBUFFER, stencilBuffer)

        // the stencil buffer shall store the depth data alongside the stencil data
        GLX.renderBufferStorage(
            GL30.GL_RENDERBUFFER,
            GL30.GL_DEPTH_STENCIL,
            framebuffer.width,
            framebuffer.height
        )

        //attach buffer as stencil and depth attachment to fbo
        GLX.frameBufferRenderBuffer(
            GL30.GL_FRAMEBUFFER,
            GL30.GL_STENCIL_ATTACHMENT,
            GL30.GL_RENDERBUFFER,
            stencilBuffer
        )
        GLX.frameBufferRenderBuffer(
            GL30.GL_FRAMEBUFFER,
            GL30.GL_DEPTH_ATTACHMENT,
            GL30.GL_RENDERBUFFER,
            stencilBuffer
        )

        GL11.glClearStencil(0)
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT)
        GL11.glEnable(GL11.GL_STENCIL_TEST)

        GL11.glStencilFunc(GL11.GL_ALWAYS, 1, -1)
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_REPLACE, GL11.GL_REPLACE)
    }

    /**
     * Apply the stencil buffer onto every following render call until a call to [endStencil]
     *
     * @param mode [StencilMode] to be applied
     */
    fun enableStencil(mode: StencilMode) {
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_KEEP)
        GL11.glStencilFunc(mode.maskBit, 0, -1)
    }

    fun endStencil() {
        GL11.glStencilFunc(GL11.GL_ALWAYS, 0, -1)
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_REPLACE, GL11.GL_REPLACE)
    }

    /**
     * Stencil mode defines whether to delete pixels inside the stencil buffer mask [CROP_INSIDE] or outside the mask [CROP_OUTSIDE]
     */
    enum class StencilMode(internal val maskBit: Int) {
        CROP_INSIDE(GL11.GL_EQUAL), CROP_OUTSIDE(GL11.GL_NOTEQUAL)
    }
}
