package infra.core.bus

import net.techcable.event4j.EventBus
import net.techcable.event4j.EventExecutor


/**
 * Central event bus that decides how and where to dispatch events
 */
object EventBroker {

    private val eventBus = EventBus.builder()
        .executorFactory(
            EventExecutor.Factory.ASM_LISTENER_FACTORY
                .orElse(EventExecutor.Factory.REFLECTION_LISTENER_FACTORY)
        )
        .build()

    fun dispatchEvent(e: Event) {
        eventBus.fire(e)
    }

    fun registerListener(listener: EventListener) {
        eventBus.register(listener)
    }

    fun unregisterListener(listener: EventListener) {
        eventBus.unregister(listener)
    }
}