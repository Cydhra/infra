package infra.core.bus

/**
 * Cancellable event
 */
interface Cancellable {

    /**
     * Whether the event has been cancelled
     */
    var cancelled: Boolean
}