package infra.core.bus

/**
 * An event of the client-internal event system that is used to interact with the minecraft core.
 */
interface Event {
}