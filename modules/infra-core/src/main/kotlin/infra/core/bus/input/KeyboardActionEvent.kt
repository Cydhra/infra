package infra.core.bus.input

import infra.core.bus.Cancellable
import infra.core.bus.CancellableEvent
import infra.core.bus.Event

/**
 * Fired whenever a key state changes
 *
 * @param action the change of state of the key
 * @param keyCode the changed key
 */
class KeyboardActionEvent(val action: KeyboardAction, val keyCode: Int) : Event, Cancellable by CancellableEvent() {

    enum class KeyboardAction {
        PRESS, RELEASE
    }
}