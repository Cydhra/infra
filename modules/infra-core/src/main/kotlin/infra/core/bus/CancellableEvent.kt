package infra.core.bus

/**
 * Delegated implementation of [Cancellable]
 */
class CancellableEvent() : Cancellable {
    override var cancelled: Boolean = false
}