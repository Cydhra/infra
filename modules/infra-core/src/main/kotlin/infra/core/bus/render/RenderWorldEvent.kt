package infra.core.bus.render

import infra.core.bus.Event

class RenderWorldEvent(val partialTicks: Float) : Event