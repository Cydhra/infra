package infra.core.system

/**
 * A strategy for loading modules from any source.
 */
interface ModuleLoader {

    /**
     * Is called by the loader subsystem to load modules for all subsystems including the loader itself
     */
    fun loadModules()
}