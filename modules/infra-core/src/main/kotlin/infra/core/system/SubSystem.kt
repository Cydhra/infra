package infra.core.system

/**
 * A subsystem of the infra client. Each subsystem is responsible for one client functionality and can take modules
 * from any source of modules.
 *
 * @param M type of module this subsystem can load
 */
interface SubSystem<M> {

    /**
     * Initialize the module system
     */
    fun init()

    /**
     * Register a module at the subsystem extending the functionality it provides.
     *
     * @param module the module to register. It can be initialized at will
     */
    fun registerModule(module: M)

    /**
     * Get a list of all modules currently registered at this subsystem.
     */
    fun getModules(): List<M>
}