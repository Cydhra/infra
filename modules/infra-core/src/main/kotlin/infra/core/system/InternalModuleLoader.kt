package infra.core.system

import infra.core.modulesystem.ModuleManager
import infra.core.modulesystem.modules.DebugModule

/**
 * Internal module loader responsible for loading all modules provided by the core system
 */
object InternalModuleLoader : ModuleLoader {
    override fun loadModules() {
        ModuleManager.registerModule(DebugModule())
    }
}