package infra.core.gui

import infra.api.client.gui.InfraGuiScreen
import infra.core.gui.controller.DisplayController
import infra.core.gui.theme.Theme

/**
 * A display provider for the GUI system provides the display server with an actual graphical representation for a
 * given controller. The provider must be able to automatically generate a  GUI from a controller, but may override
 * certain GUIs with custom representations, as long as the gui controller specification is fulfilled.
 *
 * @param T the theme type
 */
abstract class DisplayProvider<T : Theme> {

    /**
     * @return a list of available themes
     */
    abstract fun getThemes(): List<T>

    /**
     * Provide an instance of [InfraGuiScreen] that displays a graphical interface to the given controller. How the
     * interface is generated is up to the provider, but it must not return an invalid interface, even if the
     * controller or its purpose is unknown to the provider.
     */
    abstract fun provide(controller: DisplayController, theme: T): InfraGuiScreen
}