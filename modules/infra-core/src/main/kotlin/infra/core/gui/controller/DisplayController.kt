package infra.core.gui.controller

/**
 * Controller for a graphical user interface displayed within the client. The actual [infra.core.gui.DisplayProvider]
 * chooses how to display the functionality provided by this controller.
 */
abstract class DisplayController