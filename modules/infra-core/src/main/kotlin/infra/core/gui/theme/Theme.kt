package infra.core.gui.theme

/**
 * Common interface for themes. As themes are specific for their provider, their implementation is up to the provider.
 */
interface Theme {
    /**
     * Display name for the theme
     */
    val name: String
}