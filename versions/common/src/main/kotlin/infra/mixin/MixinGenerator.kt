package infra.mixin

import infra.api.client.InfraMinecraft
import infra.api.client.gui.InfraGuiButton
import infra.api.client.gui.InfraGuiScreen
import infra.api.client.gui.InfraWindow
import infra.api.entity.InfraEntity
import infra.api.entity.InfraEntityLiving
import infra.api.entity.player.InfraPlayer
import infra.api.entity.player.InfraPlayerClient
import infra.api.render.GLX
import infra.api.render.GlStateManager
import infra.api.render.InfraRenderGlobal
import infra.api.render.camera.InfraFrustum
import infra.api.render.entity.InfraEntityRenderDispatcher
import infra.api.render.shader.InfraFramebuffer
import infra.api.render.tessellator.DefaultVertexFormats
import infra.api.render.tessellator.InfraTessellator
import infra.api.render.tessellator.InfraVertexBufferBuilder
import infra.api.render.tessellator.InfraVertexFormat
import infra.api.text.InfraBaseText
import infra.api.text.InfraLiteralText
import infra.api.text.InfraLiteralTextSurrogate
import infra.api.text.InfraText
import infra.api.util.math.InfraBlockPos
import infra.api.util.math.InfraBlockPosSurrogate
import infra.api.util.math.InfraBoundingBox
import infra.api.util.math.InfraBoundingBoxSurrogate
import infra.api.world.InfraWorldClient
import infra.processors.injection.GenerateInjections
import infra.processors.redirect.GenerateRedirects
import infra.processors.surrogate.GenerateSurrogateMixins

/**
 * A dummy object that is just use to invoke annotation processing for [GenerateInjections]. The annotation lists
 * every class that shall be injected into the platform.
 */
@GenerateInjections(
    InfraMinecraft::class,
    InfraGuiScreen::class,
    InfraGuiButton::class,
    InfraEntity::class,
    InfraEntityLiving::class,
    InfraPlayer::class,
    InfraPlayerClient::class,
    InfraRenderGlobal::class,
    InfraFrustum::class,
    InfraEntityRenderDispatcher::class,
    InfraFramebuffer::class,
    InfraTessellator::class,
    InfraVertexBufferBuilder::class,
    InfraVertexFormat::class,
    InfraBaseText::class,
    InfraLiteralText::class,
    InfraText::class,
    InfraBlockPos::class,
    InfraBoundingBox::class,
    InfraWorldClient::class
)
@GenerateSurrogateMixins(
    InfraLiteralTextSurrogate::class,
    InfraBlockPosSurrogate::class,
    InfraBoundingBoxSurrogate::class
//    InfraGuiButtonSurrogate::class
)
@GenerateRedirects(
    InfraWindow::class,
    DefaultVertexFormats::class,
    GlStateManager::class,
    GLX::class
)
object MixinGenerator