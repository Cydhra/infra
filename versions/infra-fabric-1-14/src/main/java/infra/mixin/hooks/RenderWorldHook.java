package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.render.RenderWorldEvent;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.GameRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GameRenderer.class)
public class RenderWorldHook {

    @Inject(method = "renderHand", at = @At("RETURN"))
    protected void onRenderHand(Camera camera, float partialTicks, CallbackInfo info) {
        EventBroker.INSTANCE.dispatchEvent(new RenderWorldEvent(partialTicks));
    }
}
