package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.client.MinecraftTickEvent;
import net.minecraft.client.MinecraftClient;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
public class MinecraftTickHook {

    @Inject(method = "tick", at = @At("RETURN"))
    protected void onTickGame(CallbackInfo info) {
        EventBroker.INSTANCE.dispatchEvent(new MinecraftTickEvent());
    }
}
