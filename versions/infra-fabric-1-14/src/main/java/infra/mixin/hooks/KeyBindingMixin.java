package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.input.KeyboardActionEvent;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@SuppressWarnings({"UnusedAssignment", "unused"})
@Mixin(KeyBinding.class)
public abstract class KeyBindingMixin {

    @Inject(method = "setKeyPressed", at = @At("HEAD"), cancellable = true)
    private static void onSetKeyBindState(InputUtil.KeyCode keyCode, boolean pressed, final CallbackInfo info) {
        if (keyCode.getKeyCode() != 0) {
            final KeyboardActionEvent event = new KeyboardActionEvent(pressed ? KeyboardActionEvent.KeyboardAction.PRESS
                    : KeyboardActionEvent.KeyboardAction.RELEASE, keyCode.getKeyCode());
            EventBroker.INSTANCE.dispatchEvent(event);
            if (event.getCancelled()) {
                info.cancel();
            } else {
                keyCode = InputUtil.getKeyCode(-1, event.getKeyCode());
                pressed = event.getAction() == KeyboardActionEvent.KeyboardAction.PRESS;
            }
        }
    }
}
