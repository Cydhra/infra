package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.render.RenderOverlayEvent;
import net.minecraft.client.gui.hud.InGameHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(InGameHud.class)
public class RenderOverlayHook {

    @Inject(method = "render", at = @At("TAIL"))
    protected void onRenderGameOverlay(float partialTicks, CallbackInfo info) {
        EventBroker.INSTANCE.dispatchEvent(new RenderOverlayEvent(partialTicks));
    }
}
