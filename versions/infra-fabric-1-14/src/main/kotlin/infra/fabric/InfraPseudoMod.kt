@file:Suppress("unused")

package infra.fabric

import org.apache.logging.log4j.LogManager

/**
 * Pseudo mod that is loaded by fabric. The actual client is initialized via a mixin hook.
 */
class InfraPseudoMod {

    fun init() {
        LogManager.getLogger().info("Infra Pseudo Mod registered")
    }
}