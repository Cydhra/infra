package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.input.KeyboardActionEvent;
import net.minecraft.client.settings.KeyBinding;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@SuppressWarnings({"UnusedAssignment", "unused"})
@Mixin(KeyBinding.class)
public abstract class KeyBindingMixin {

    @Inject(method = "setKeyBindState", at = @At("HEAD"), cancellable = true)
    private static void onSetKeyBindState(int keyCode, boolean pressed, final CallbackInfo info) {
        if (keyCode != 0) {
            final KeyboardActionEvent event = new KeyboardActionEvent(pressed ? KeyboardActionEvent.KeyboardAction.PRESS
                    : KeyboardActionEvent.KeyboardAction.RELEASE, keyCode);
            EventBroker.INSTANCE.dispatchEvent(event);
            if (event.getCancelled()) {
                info.cancel();
            } else {
                keyCode = event.getKeyCode();
                pressed = event.getAction() == KeyboardActionEvent.KeyboardAction.PRESS;
            }
        }
    }
}
