package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.client.MinecraftTickEvent;
import net.minecraft.client.Minecraft;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Minecraft.class)
public class MinecraftTickHook {

    @Inject(method = "runTick", at = @At("RETURN"))
    protected void onTickGame(CallbackInfo info) {
        EventBroker.INSTANCE.dispatchEvent(new MinecraftTickEvent());
    }
}
