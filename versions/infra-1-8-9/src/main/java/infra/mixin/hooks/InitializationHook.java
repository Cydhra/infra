package infra.mixin.hooks;

import infra.api.client.InfraMinecraft;
import infra.core.InfraClient;
import net.minecraft.client.Minecraft;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@SuppressWarnings({"unused"})
@Mixin(Minecraft.class)
public abstract class InitializationHook {

    @Inject(method = "startGame", at = @At("RETURN"))
    protected void onStartGame(CallbackInfo info) {
        InfraClient.INSTANCE.init((InfraMinecraft) Minecraft.getMinecraft());
    }
}
