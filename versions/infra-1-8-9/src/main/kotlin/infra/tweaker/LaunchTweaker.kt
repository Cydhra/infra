package infra.tweaker

import net.minecraft.launchwrapper.ITweaker
import net.minecraft.launchwrapper.LaunchClassLoader
import org.spongepowered.asm.launch.MixinBootstrap
import org.spongepowered.asm.mixin.MixinEnvironment
import org.spongepowered.asm.mixin.MixinEnvironment.Side.CLIENT
import org.spongepowered.asm.mixin.Mixins
import java.io.File

private const val MINECRAFT_MAIN_CLASS = "net.minecraft.client.main.Main"
private const val MIXIN_CONFIG = "mixins.infra.json"
private const val MIXIN_AUTO_CONFIG = "automixins.infra.json"
private const val MIXIN_SURROGATES = "autosurrogates.infra.json"
private const val MIXIN_REDIRECTS = "autoredirects.infra.json"

/**
 * [ITweaker] implementation of the Minecraft Launchwrapper API that enables the Mixin bootstrap thus invoking class merging.
 */
class LaunchTweaker : ITweaker {

    private lateinit var arguments: Array<String>

    override fun acceptOptions(
        args: MutableList<String>,
        gameDirectory: File?,
        assetsDirectory: File?,
        version: String?
    ) {
        this.arguments = mutableListOf<String>()
            .apply { addAll(args) }
            .also {
                if (gameDirectory != null) {
                    it += "--gameDir"
                    it += gameDirectory.absolutePath
                }

                if (assetsDirectory != null) {
                    it += "--assetsDir"
                    it += assetsDirectory.absolutePath
                }

                if (version != null) {
                    it += "--version"
                    it += version
                }
            }
            .toTypedArray()
    }

    override fun getLaunchTarget(): String {
        return MINECRAFT_MAIN_CLASS
    }

    override fun injectIntoClassLoader(classLoader: LaunchClassLoader) {
        MixinBootstrap.init()
        Mixins.addConfiguration(MIXIN_CONFIG)
        Mixins.addConfiguration(MIXIN_AUTO_CONFIG)
        Mixins.addConfiguration(MIXIN_SURROGATES)
        Mixins.addConfiguration(MIXIN_REDIRECTS)
        MixinEnvironment.getDefaultEnvironment().side = CLIENT
    }

    override fun getLaunchArguments(): Array<String> {
        return arguments
    }

}